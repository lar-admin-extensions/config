<?php

return [
    "name" => "config",
    "group" => "config",
    "path" => "lar-admin-extensions/config",
    "description" => "Application for storing settings in a database.",
    "version" => "1.0.0",
    "icon" => "nut",
    "namespace" => "Lar\\Admin\\Config",
    "class" => "Config",
    "repo" => "https://lar-admin-extensions@bitbucket.org/lar-admin-extensions/config.git"
];
