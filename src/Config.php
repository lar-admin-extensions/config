<?php

namespace Lar\Admin\Config;

use Lar\Admin\Commands\ExtendInstall;
use Lar\Admin\Commands\ExtendRemove;
use Lar\Admin\Commands\Update;
use Lar\Admin\Config\Models\ConfigGroup;
use Lar\Layout\Commands\HeaderDumpUpdate;

/**
 * Config Class
 * 
 * @package Lar\Admin\Config
 */
class Config
{
    /**
     * Load configure into laravel from database.
     *
     * @return void
     */
    public static function load()
    {
        foreach (ConfigGroup::where('active', 1)->get(['id', 'slug']) as $item) {

            foreach ($item->activeConfigs as $config) {

                config([$item->slug . "." . $config->slug => $config->value]);
            }
        }
    }

    /**
     * @param Update $update
     */
    static function update(Update $update) {

        $update->info('Extension [Config] updated!');
    }

    /**
     * @param HeaderDumpUpdate $dump
     */
    static function dump(HeaderDumpUpdate $dump) {

        $dump->info('Extension [Config] dumped!');
    }

    /**
     * Static method install
     *
     * @param ExtendInstall $installer
     * @return void
     */
    static function install(ExtendInstall $installer) {

        $installer->info('Extension [Config] installed!');
    }

    /**
     * Static method uninstall
     *
     * @param ExtendRemove $uninstaller
     * @return void
     */
    static function uninstall(ExtendRemove $uninstaller) {

        $uninstaller->info('Extension [Config] run uninstall...');
    }
}
