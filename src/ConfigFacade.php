<?php

namespace Lar\Admin\Config;

use Illuminate\Support\Facades\Facade;

/**
 * ConfigFacade Class
 * 
 * @package Lar\Admin\Config
 */
class ConfigFacade extends Facade
{
    /**
     * Protected Static method getFacadeAccessor
     * 
     * @return void
     */
    protected static function getFacadeAccessor() {
        return Config::class;
    }

}
