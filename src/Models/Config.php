<?php

namespace Lar\Admin\Config\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Config
 *
 * @package Lar\Admin\Config\Models
 */
class Config extends Model {

    /**
     * @var string
     */
    protected $table = "admin_configs";

    /**
     * @var array
     */
    protected $fillable = [
        'order', 'config_group_id', 'type', 'name', 'slug', 'value', 'description', 'props', 'roles', 'active'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'order' => 'int',
        'config_group_id' => 'int',
        'type' => 'string',
        'name' => 'string',
        'slug' => 'string',
        'description' => 'string',
        'props' => 'array',
        'roles' => 'array',
        'active' => 'boolean'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function group()
    {
        return $this->hasOne(ConfigGroup::class, 'id', 'config_group_id');
    }
}
