<?php

namespace Lar\Admin\Config\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Config
 *
 * @package Lar\Admin\Config\Models
 */
class ConfigGroup extends Model {

    /**
     * @var string
     */
    protected $table = "admin_config_groups";

    /**
     * @var array
     */
    protected $fillable = [
        'order', 'name', 'slug', 'description', 'roles', 'hide', 'active'
    ];

    /**
     * @var array
     */
    protected $casts = [
        'order' => 'int',
        'name' => 'string',
        'slug' => 'string',
        'description' => 'string',
        'roles' => 'array',
        'hide' => 'boolean',
        'active' => 'boolean'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function configs()
    {
        return $this->hasMany(Config::class, 'config_group_id', 'id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function activeConfigs()
    {
        return $this->hasMany(Config::class, 'config_group_id', 'id')->where('active', 1);
    }
}
