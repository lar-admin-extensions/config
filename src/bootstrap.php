<?php

use \Lar\Admin\Admin;
use \Lar\Layout\Respond;

return function (Admin $admin, Respond $respond) {

    $admin->addApp("admin.options", [
        "order" => 998,
        "type" => 'open',
        "parent_id" => "admin",
        "title" => "Options",
        "icon" => "settings",
        "component" => \Lar\Admin\Config\Scenes\ConfigScene::class
    ]);
};
