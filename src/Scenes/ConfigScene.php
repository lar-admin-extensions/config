<?php

namespace Lar\Admin\Config\Scenes;

use Illuminate\Support\Str;
use Lar\Admin\Components\AdminUi\AdminUiButton\AdminUiButtonGroup\AdminUiButtonGroup;
use Lar\Admin\Config\Models\Config;
use Lar\Admin\Config\Models\ConfigGroup;
use Lar\Admin\Core\SceneBase;
use Lar\Admin\Models\AdminRole;
use Lar\Admin\StageStudy\CardFormStage;
use Lar\Admin\StageStudy\CardStage;
use Lar\Admin\StageStudy\FormStage;
use Lar\Admin\StageStudy\GridStage;
use Lar\Admin\StageStudy\TabStage;
use Lar\Layout\Tags\CODE;
use Lar\Layout\Tags\TD;
use Lar\Layout\Tags\TR;

/**
 * Class ConfigScene
 *
 * @package Lar\Admin\Config\Scenes
 */
class ConfigScene extends SceneBase {

    use ConfigActions;

    public function index(CardStage $card, TabStage $tab)
    {
        $card->title("Config list");

        if (\Admin::user()->isRoot()) {

            $card->badge()->admin_ui_button_group(function (AdminUiButtonGroup $group) {

                $group->small_icon_button('plus', 'Add config')->primary()
                    ->lj('click')->setMethod('form');

                $group->small_icon_button('plus', 'Add group')->primary()
                    ->lj('click')->setMethod('form_group');
            });
        }

        foreach (ConfigGroup::where('active', 1)->get() as $item) {

            $tab->tab(function (ConfigGroup $configGroup, GridStage $grid) {

                $grid->h3($configGroup->description);

                $grid->setModel($configGroup->configs());

                if (\Admin::user()->isRoot()) {

                    $grid->column('Config', function (Config $config) use ($configGroup) {

                        return CODE::create("config('{$configGroup->slug}.{$config->slug}')");
                    });
                }

                $grid->column('Description', 'description');

                $grid->column('Value', 'editable:value,type');

                $grid->column('Updated At', 'updated_at', true);

                if (\Admin::user()->isRoot()) {

                    $grid->column("Tools", function (Config $config, TD $td, TR $tr) {

                        $td->admin_ui_button_group(function (AdminUiButtonGroup $group) use ($config, $td, $tr) {

                            $group->small_icon_button('pencil')->primary()
                                ->lj("click")
                                ->setMethod("form", [Config::class => $config->id])->tooltip('Edit ' . $config->name);

                            $group->small_icon_button('trash')->danger()
                                ->lj("click")
                                ->ui_confirm("Delete {$config->name}: {$config->description} [ID:{$config->id}]?")
                                ->action("delete_config", ["config" => $config->id]);
                        });
                    });
                }

                $grid->cardFooter();

            }, $item->name, [ConfigGroup::class => $item]);
        }
    }

    /**
     * @param CardFormStage $form
     * @param Config $config
     */
    public function form(CardFormStage $form, Config $config)
    {
        $form->id();

        $form->number("order", "Order")->rule("int|min:0")->default(Config::count());

        $form->select("config_group_id", "Group")
            ->options(ConfigGroup::query()->where('active', 1), "id:name")
            ->rule('required|exists:admin_config_groups,id');

        if (!$config->exists) {

            $form->select('type', 'Type')
                ->options(FormStage::listExtension())->tpl('{id} - {text}')->tpl_select('tpl')
                ->rule('required');

            $form->txt('name', 'Name');
        }

        else {

            $form->display('type', 'Type');

            $form->{$config->type}('value', 'Value');
        }

        $form->textarea('description', 'Description');

        $form->select('props', 'Props')->tags();

        if (\Admin::user()->isRoot()) {

            $form->select('roles', 'Roles')->options(AdminRole::query()->where('active', 1), "slug:name")->multiple();
        }

        $form->save(function (array $data) {

            $data['slug'] = Str::slug($data['name'], '_');

            return $data;
        });
    }

    /**
     * @param CardFormStage $form
     * @param ConfigGroup $group
     */
    public function form_group(CardFormStage $form, ConfigGroup $group)
    {
        $form->id();

        $form->number("order", "Order")->rule("int|min:0")->default(ConfigGroup::count());

        $form->txt('name', 'Name');

        $form->textarea('description', 'Description');

        $form->select('roles', 'Roles')->options(AdminRole::query()->where('active', 1), "slug:name")->multiple();

        $form->timestamps();

        $form->save(function (array $data) {

            $data['slug'] = Str::slug($data['name'], '_');

            return $data;
        });
    }
}
