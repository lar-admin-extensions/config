<?php

namespace Lar\Admin\Config\Scenes;

use Lar\Admin\Config\Models\Config;
use Lar\Layout\Respond;

/**
 * Trait ConfigActions
 *
 * @package Lar\Admin\Config\Scenes
 */
trait ConfigActions
{

    public function delete_config(Config $config, Respond $respond)
    {
        if ($config->exists) {

            if ($config->delete()) {

                $respond->timeOut(500)->updateContent()->toast_success("Config [{$config->name}] deleted!");
            }

            else {

                $respond->toast_error("Undefined error on delete config [{$config->name}]!");
            }
        }

        else {

            $respond->toast_error("Undefined config!");
        }
    }
}
