<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminConfigsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_configs', function (Blueprint $table) {

            $table->increments('id');

            $table->bigInteger('order')->default('0');

            $table->unsignedInteger('config_group_id');

            $table->string('type')->default('txt');

            $table->string('name');

            $table->string('slug');

            $table->longText('value')->nullable();

            $table->text('description')->nullable();

            $table->json('props')->nullable();

            $table->json('roles')->nullable();

            $table->boolean('active')->default(1);

            $table->timestamps();

            $table->foreign('config_group_id')->references('id')->on('admin_config_groups');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_configs');
    }
}
