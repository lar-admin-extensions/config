<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateAdminConfigGroupsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_config_groups', function (Blueprint $table) {

            $table->increments('id');

            $table->bigInteger('order')->default('0');

            $table->string('name');

            $table->string('slug');

            $table->text('description')->nullable();

            $table->json('roles')->nullable();

            $table->boolean('hide')->default(1);

            $table->boolean('active')->default(1);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admin_config_groups');
    }
}
